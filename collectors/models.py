import requests
import json
import datetime

class Model(object):
    def __init__(self):
        self.fields = {}

    def load_json(self, json):
        for field_name in self.field_names:
            if field_name in json.keys():
                self.fields[field_name] = json[field_name]

    def load_server(self, id):
        url = 'http://localhost:80/api/v1/{0}/{1}'.format(self.model_path, id)
        headers = {'content-type': 'application/json; charset=UTF-8'}
        r = requests.get(url, headers=headers)
        if r.status_code == 200:
            self.load_json(r.json()[self.model_name])
            return True
        return False

    def set(self, field, value):
        if field in self.field_names:
            self.fields[field] = value

    def get(self, field):
        if field in self.field_names:
            if field in self.fields.keys():
                return self.fields[field]
        return None

    def save(self):
        if self.get('id') == None:
            return self._create()
        return self._update()

    def _update(self):
        url = 'http://localhost:80/api/v1/{0}/{1}'.format(self.model_path, self.get('id'))
        data = json.dumps({self.model_name: self.fields})
        headers = {'content-type': 'application/json; charset=UTF-8'}
        r = requests.put(url, data=data, headers=headers)
        if r.status_code == 200:
            self.load_json(r.json()[self.model_name])
            return True
        print r.text
        return False

    def _create(self):
        url = 'http://localhost:80/api/v1/{0}'.format(self.model_path)
        data = json.dumps({self.model_name: self.fields})
        headers = {'content-type': 'application/json; charset=UTF-8'}
        r = requests.post(url, data=data, headers=headers)
        if r.status_code == 201:
            self.load_json(r.json()[self.model_name])
            return True
        print r.text
        return False

    def __str__(self):
        return '<{0} [id={1}]>'.format(self.__class__.__name__,  self.get('id'))

class Server(Model):
    model_name = 'server'
    model_path = 'servers'
    field_names = ('id', 'ip_address', 'local_hostname', 'hostname', 'monit_id', 'monit_port', 'monit_ssl', 'monit_username', 'monit_password', 'monit_uptime', 'monit_poll', 'platform_name', 'platform_release', 'platform_version', 'platform_architecture', 'cpu_count', 'memory_size', 'swap_size', 'system_load_avg01', 'system_load_avg05', 'system_load_avg15', 'system_cpu_user', 'system_cpu_system', 'system_cpu_wait', 'system_memory_used', 'system_swap_used', 'collected_at', 'created_at', 'updated_at', 'processes')

    def load_xml(self, server_xml, platform_xml, system_xml):
        self.set('ip_address', server_xml.find('httpd').find('address').text)
        self.set('local_hostname', server_xml.find('localhostname').text)
        self.set('monit_id', server_xml.find('id').text)
        self.set('monit_port', server_xml.find('httpd').find('port').text)
        self.set('monit_ssl', server_xml.find('httpd').find('ssl').text)
        self.set('monit_uptime', server_xml.find('uptime').text)
        self.set('monit_poll', server_xml.find('poll').text)
        self.set('platform_name', platform_xml.find('name').text)
        self.set('platform_release', platform_xml.find('release').text)
        self.set('platform_version', platform_xml.find('version').text)
        self.set('platform_architecture', platform_xml.find('machine').text)
        self.set('cpu_count', platform_xml.find('cpu').text)
        self.set('memory_size', platform_xml.find('memory').text)
        self.set('swap_size', platform_xml.find('swap').text)
        self.set('system_load_avg01', system_xml.find('system').find('load').find('avg01').text)
        self.set('system_load_avg05', system_xml.find('system').find('load').find('avg05').text)
        self.set('system_load_avg15', system_xml.find('system').find('load').find('avg15').text)
        self.set('system_cpu_user', system_xml.find('system').find('cpu').find('user').text)
        self.set('system_cpu_system', system_xml.find('system').find('cpu').find('system').text)
        self.set('system_cpu_wait', system_xml.find('system').find('cpu').find('wait').text)
        self.set('system_memory_used', system_xml.find('system').find('memory').find('kilobyte').text)
        self.set('system_swap_used', system_xml.find('system').find('swap').find('kilobyte').text)
        # self.set('collected_at', system_xml.find('system').find('bla').text)


class Process(Model):
    model_name = 'process'
    model_path = 'processes'
    field_names = ('id', 'name', 'server', 'status_ok', 'status_state', 'status_message', 'pid', 'ppid', 'uptime', 'child_count', 'memory_percent', 'memory_percent_total', 'memory_kilobyte', 'memory_kilobyte_total', 'cpu_percent', 'cpu_percent_total', 'collected_at', 'created_at', 'updated_at', 'systems')

    def load_xml(self, xml):
        self.set('name', xml.find('name').text)
        self.set('status_state', xml.find('status').text)
        # self.set('collected_at', xml.find('bla').text)
        if xml.find('status').text == '0':
            self.set('status_ok', 1)
            self.set('status_message', 'process running')
            self.set('pid', xml.find('pid').text)
            self.set('ppid', xml.find('ppid').text)
            self.set('uptime', xml.find('uptime').text)
            self.set('child_count', xml.find('children').text)
            self.set('memory_percent', xml.find('memory').find('percent').text)
            self.set('memory_percent_total', xml.find('memory').find('percenttotal').text)
            self.set('memory_kilobyte', xml.find('memory').find('kilobyte').text)
            self.set('memory_kilobyte_total', xml.find('memory').find('kilobytetotal').text)
            self.set('cpu_percent', xml.find('cpu').find('percent').text)
            self.set('cpu_percent_total', xml.find('cpu').find('percenttotal').text)

        else:
            self.set('status_ok', 0)
            self.set('status_message', xml.find('status_message').text)
            self.set('pid',None)
            self.set('ppid',None)
            self.set('uptime',None)
            self.set('child_count',None)
            self.set('memory_percent',None)
            self.set('memory_percent_total',None)
            self.set('memory_kilobyte',None)
            self.set('memory_kilobyte_total',None)
            self.set('cpu_percent',None)
            self.set('cpu_percent_total',None)


