import requests
from lxml import etree
from models import Server, Process

def getXmlString(ip, port, username, password):
    try:
        r = requests.get('http://{0}:{1}/_status?format=xml'.format(ip, port), auth=(username, password), timeout=1)
        if r.status_code == 200:
            return etree.fromstring(r.text.encode('utf-8'))
        return None
    except requests.exceptions.ConnectionError as connectionError:
        print connectionError
        return None

try:
    r = requests.get('http://localhost:80/api/v1/servers', params={'page_size': 0})
    for server_json in r.json()['server']:
        xml = getXmlString(server_json['ip_address'],
                           server_json['monit_port'],
                           server_json['monit_username'],
                           server_json['monit_password'])

        if xml != None:
            # Variables to hold the XML nodes
            server_node = xml.find('server')
            platform_node = xml.find('platform')
            filesystem_nodes = []
            directory_nodes = []
            file_nodes = []
            process_nodes = []
            host_nodes = []
            system_node = None

            # File the above variables with the correct XML nodes.
            for service_node in xml.findall('service'):
                type = int(service_node.get('type'))
                if type == 0:
                    filesystem_nodes.append(service_node)
                if type == 1:
                    directory_nodes.append(service_node)
                if type == 2:
                    file_nodes.append(service_node)
                if type == 3:
                    process_nodes.append(service_node)
                if type == 4:
                    host_nodes.append(service_node)
                if type == 5:
                    system_node = service_node

            # Create a server object with the data return from the dashboard
            server = Server()
            server.load_json(server_json)
            server.load_xml(server_node, platform_node, system_node)
            server.save()

            processes = []

            for process_id in server.get('processes'):
                process = Process()
                process.load_server(process_id)
                processes.append(process)

            for process_node in process_nodes:
                name = process_node.find('name').text
                process = None
                for possible in processes:
                    if possible.get('name') == name:
                        process = possible
                if process == None:
                    process = Process()
                    process.set('server', server.get('id'))
                process.load_xml(process_node)
                process.save()

except requests.exceptions.ConnectionError as connectionError:
        print connectionError
