from django.db import models

class Server(models.Model):
    ip_address = models.IPAddressField(blank=False)
    local_hostname = models.CharField(max_length=100, blank=True)
    hostname = models.CharField(max_length=200, blank=False)
    monit_id = models.CharField(max_length=100, blank=True)
    monit_port = models.IntegerField(blank=False)
    monit_ssl = models.NullBooleanField()
    monit_username = models.CharField(max_length=100, blank=False)
    monit_password = models.CharField(max_length=100, blank=False)
    monit_uptime = models.BigIntegerField(blank=True, null=True)
    monit_poll = models.IntegerField(blank=True, null=True)
    platform_name = models.CharField(max_length=100, blank=True)
    platform_release = models.CharField(max_length=100, blank=True)
    platform_version = models.CharField(max_length=100, blank=True)
    platform_architecture = models.CharField(max_length=100, blank=True)
    cpu_count = models.IntegerField(blank=True, null=True)
    memory_size = models.BigIntegerField(blank=True, null=True)
    swap_size = models.BigIntegerField(blank=True, null=True)
    system_load_avg01 = models.FloatField(null=True, blank=True)
    system_load_avg05 = models.FloatField(null=True, blank=True)
    system_load_avg15 = models.FloatField(null=True, blank=True)
    system_cpu_user = models.FloatField(null=True, blank=True)
    system_cpu_system = models.FloatField(null=True, blank=True)
    system_cpu_wait = models.FloatField(null=True, blank=True)
    system_memory_used = models.IntegerField(blank=True, null=True)
    system_swap_used = models.IntegerField(blank=True, null=True)
    collected_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "<Server {0}>".format(self.local_hostname)

class Process(models.Model):
    name = models.CharField(max_length=100, blank=False)
    server = models.ForeignKey(Server, related_name='processes')
    status_ok = models.BooleanField(default=False)
    status_state = models.CharField(max_length=10, blank=True)
    status_message = models.CharField(max_length=100, blank=True)
    pid = models.IntegerField(blank=True, null=True)
    ppid = models.IntegerField(blank=True, null=True)
    uptime = models.BigIntegerField(blank=True, null=True)
    child_count = models.IntegerField(blank=True, null=True)
    memory_percent = models.FloatField(null=True, blank=True)
    memory_percent_total = models.FloatField(null=True, blank=True)
    memory_kilobyte = models.IntegerField(blank=True, null=True)
    memory_kilobyte_total = models.IntegerField(blank=True, null=True)
    cpu_percent = models.FloatField(null=True, blank=True)
    cpu_percent_total = models.FloatField(null=True, blank=True)
    collected_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "<Process {0}>".format(self.name)


class System(models.Model):
    name = models.CharField(max_length=100, blank=False)
    processes = models.ManyToManyField(Process, related_name='systems')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "<System {0}>".format(self.name)
