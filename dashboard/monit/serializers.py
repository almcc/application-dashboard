from django.forms import widgets
from rest_framework import serializers
from monit.models import Server, Process, System

class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Server
        fields = ('id', 'ip_address', 'local_hostname', 'hostname', 'monit_id', 'monit_port', 'monit_ssl', 'monit_username', 'monit_password', 'monit_uptime', 'monit_poll', 'platform_name', 'platform_release', 'platform_version', 'platform_architecture', 'cpu_count', 'memory_size', 'swap_size', 'system_load_avg01', 'system_load_avg05', 'system_load_avg15', 'system_cpu_user', 'system_cpu_system', 'system_cpu_wait', 'system_memory_used', 'system_swap_used', 'collected_at', 'created_at', 'updated_at', 'processes')

class ProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = ('id', 'name', 'server', 'status_ok', 'status_state', 'status_message', 'pid', 'ppid', 'uptime', 'child_count', 'memory_percent', 'memory_percent_total', 'memory_kilobyte', 'memory_kilobyte_total', 'cpu_percent', 'cpu_percent_total', 'collected_at', 'created_at', 'updated_at', 'systems')

class SystemSerializer(serializers.ModelSerializer):
    class Meta:
        model = System
        fields = ('id', 'name', 'processes', 'created_at', 'updated_at')
