from monit.models import Server, Process, System
from monit.serializers import ServerSerializer, ProcessSerializer, SystemSerializer
from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin, BroadcastMixin
from socketio.sdjango import namespace
from django.db.models.signals import post_save, post_delete, m2m_changed
from django.dispatch import receiver

connections = {}

@namespace('/monit')
class ModelsNamespace(BaseNamespace, RoomsMixin, BroadcastMixin):

    def initialize(self, *args, **kwargs):
        connections[id(self)] = self
        super(ModelsNamespace, self).initialize(*args, **kwargs)

    def disconnect(self, *args, **kwargs):
        del connections[id(self)]
        super(ModelsNamespace, self).disconnect(*args, **kwargs)


def broadcastToOne(name, data):
    if len(connections) > 0:
        aConnection = connections.values()[0]
        aConnection.broadcast_event(name, data)

@receiver(post_save, sender=Server)
def postSaveServerHandler(sender, **kwargs):
    serializer = ServerSerializer(kwargs['instance'])
    broadcastToOne('postSaveServer', serializer.data)

@receiver(post_delete, sender=Server)
def postDeleteServerHandler(sender, **kwargs):
    serializer = ServerSerializer(kwargs['instance'])
    broadcastToOne('postDeleteServer', serializer.data)

@receiver(post_save, sender=Process)
def postSaveProcessHandler(sender, **kwargs):
    serializer = ProcessSerializer(kwargs['instance'])
    broadcastToOne('postSaveProcess', serializer.data)

@receiver(post_delete, sender=Process)
def postDeleteProcessHandler(sender, **kwargs):
    serializer = ProcessSerializer(kwargs['instance'])
    broadcastToOne('postDeleteProcess', serializer.data)

@receiver(post_save, sender=System)
def postSaveSystemHandler(sender, **kwargs):
    serializer = SystemSerializer(kwargs['instance'])
    broadcastToOne('postSaveSystem', serializer.data)

@receiver(post_delete, sender=System)
def postDeleteSystemHandler(sender, **kwargs):
    serializer = SystemSerializer(kwargs['instance'])
    broadcastToOne('postDeleteSystem', serializer.data)

@receiver(m2m_changed)
def m2mChangedHandler(sender, **kwargs):
    object = kwargs['instance']
    if object.__class__ == Server:
        serializer = ServerSerializer(object)
        broadcastToOne('postSaveServer', serializer.data)
    elif object.__class__ == Process:
        serializer = ProcessSerializer(object)
        broadcastToOne('postSaveProcess', serializer.data)
    elif object.__class__ == System:
        serializer = SystemSerializer(object)
        broadcastToOne('postSaveSystem', serializer.data)
