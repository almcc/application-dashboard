from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from monit import views
# import socketio.sdjango

# socketio.sdjango.autodiscover()

urlpatterns = patterns('',
    # url("^socket\.io", include(socketio.sdjango.urls)),

    url(r'^servers$', views.ServerList.as_view()),
    url(r'^servers/(?P<pk>[0-9]+)$', views.ServerDetail.as_view()),

    url(r'^processes$', views.ProcessList.as_view()),
    url(r'^processes/(?P<pk>[0-9]+)$', views.ProcessDetail.as_view()),

    url(r'^systems$', views.SystemList.as_view()),
    url(r'^systems/(?P<pk>[0-9]+)$', views.SystemDetail.as_view()),

)

urlpatterns = format_suffix_patterns(urlpatterns)
