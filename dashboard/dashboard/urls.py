from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
import socketio.sdjango

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

socketio.sdjango.autodiscover()

urlpatterns = patterns('',
    url(r'^socket\.io', include(socketio.sdjango.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/v1/', include('monit.urls')),
    (r'^$', RedirectView.as_view(url='/static/index.html')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
