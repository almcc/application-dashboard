Helpers = {};

Helpers.advanceLoadingBar = function(advance){
    var loadingBar = $('#application-loading-bar');
    var current = parseInt(loadingBar.data('percentage'));
    var newPercent = current+advance;
    loadingBar.data('percentage', newPercent);
    loadingBar.css('width', newPercent+'%');
};
