App.ProcessRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('process', params.process_id);
    }
});