App.ServerRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('server', params.server_id);
    }
});