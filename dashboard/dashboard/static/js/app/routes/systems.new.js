App.SystemsNewRoute = Ember.Route.extend({

    setupController:function(controller, model) {
       this._super(controller, model);
       controller.set('allProcesses', App.Process.store.all('process'));
    },

    model: function() {
        return this.store.createRecord('system');
    }
});
