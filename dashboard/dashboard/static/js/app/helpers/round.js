Ember.Handlebars.helper('round-one', function(float) {
    if(float != null) {
        return float.toFixed(1);
    } else {
        return null;
    }
});

Ember.Handlebars.helper('round-two', function(float) {
    if(float != null) {
        return float.toFixed(2);
    } else {
        return null;
    }
});

Ember.Handlebars.helper('round-three', function(float) {
    if(float != null) {
        return float.toFixed(3);
    } else {
        return null;
    }
});
