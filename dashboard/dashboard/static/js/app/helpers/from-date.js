Ember.Handlebars.helper('from-date', function(date) {
    return moment(date).fromNow();
});
