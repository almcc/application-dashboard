Ember.Handlebars.helper('time-duration-seconds', function(time) {
    if(time != null) {
        return moment.duration(parseInt(time)*1000).humanize();
    } else {
        return "None";
    }

});
