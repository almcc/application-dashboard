App.SystemEditController = Ember.ObjectController.extend({
    actions: {
        cancel: function() {
            this.get('model').rollback();
            this.transitionToRoute("system", this.get('model'));
        },

        submit: function() {
            var name = this.get('name').trim();
            var system = this.get('model');
            system.set('name', name);

            var self = this;
            system.save().then(function() {
                self.transitionToRoute("system", system);
            }, function(response) {

            });
        },

        remove: function() {
            var system = this.get('model');
            var self = this;
            system.destroyRecord().then(function() {
                self.transitionToRoute("systems");
            });
        }
    },
});