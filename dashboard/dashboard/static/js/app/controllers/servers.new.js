App.ServersNewController = Ember.ObjectController.extend({
    name: "",
    actions: {
        cancel: function() {
            this.get('model').deleteRecord();
            this.transitionToRoute("servers");
        },

        submit: function() {
            var hostname = this.get('hostname').trim();
            var ip_address = this.get('ip_address').trim();
            var monit_port = this.get('monit_port')
            var monit_username = this.get('monit_username').trim();
            var monit_password = this.get('monit_password').trim();

            var server = this.get('model');
            server.set('hostname', hostname);
            server.set('ip_address', ip_address);
            server.set('monit_port', monit_port);
            server.set('monit_username', monit_username);
            server.set('monit_password', monit_password);

            var self = this;
            server.save().then(function() {
                self.transitionToRoute("server", server);
            }, function(response) {

            });
        }
    },
});
