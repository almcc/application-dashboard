App.SystemsNewController = Ember.ObjectController.extend({

    actions: {
        cancel: function() {
            this.get('model').deleteRecord();
            this.transitionToRoute("systems");
        },

        submit: function() {
            var name = this.get('name').trim();

            var system = this.get('model');
            system.set('name', name);

            var self = this;
            system.save().then(function() {
                self.transitionToRoute("system", system);
            }, function(response) {

            });
        }
    },
});
