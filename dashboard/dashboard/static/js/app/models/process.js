App.Process = DS.Model.extend({
    name: DS.attr('string'),
    status_ok: DS.attr('boolean'),
    status_state: DS.attr('string'),
    status_message: DS.attr('string'),
    pid: DS.attr('number'),
    ppid: DS.attr('number'),
    uptime: DS.attr('number'),
    child_count: DS.attr('number'),
    memory_percent: DS.attr('number'),
    memory_percent_total: DS.attr('number'),
    memory_kilobyte: DS.attr('number'),
    memory_kilobyte_total: DS.attr('number'),
    cpu_percent: DS.attr('number'),
    cpu_percent_total: DS.attr('number'),
    collected_at: DS.attr('string'),
    created_at: DS.attr('string'),
    updated_at: DS.attr('string'),
    server: DS.belongsTo('server'),
    systems: DS.hasMany('system'),

    global_name: function() {
        return this.get('name') + " - " + this.get('server').get('hostname');
    }.property('name', 'server.hostname'),

    memory_percent_total_css_width: function() {
        return 'width:' + this.get('memory_percent_total') + '%';
    }.property('memory_percent_total'),

    cpu_percent_total_css_width: function() {
        return 'width:' + this.get('cpu_percent_total') + '%';
    }.property('cpu_percent_total'),
});
