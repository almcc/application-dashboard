App.Server = DS.Model.extend({
    ip_address: DS.attr('string'),
    local_hostname: DS.attr('string'),
    hostname: DS.attr('string'),
    monit_id: DS.attr('string'),
    monit_port: DS.attr('number'),
    monit_ssl: DS.attr('boolean'),
    monit_username: DS.attr('string'),
    monit_password: DS.attr('string'),
    monit_uptime: DS.attr('number'),
    monit_poll: DS.attr('number'),
    platform_name: DS.attr('string'),
    platform_release: DS.attr('string'),
    platform_version: DS.attr('string'),
    platform_architecture: DS.attr('string'),
    cpu_count: DS.attr('number'),
    memory_size: DS.attr('number'),
    swap_size: DS.attr('number'),
    system_load_avg01: DS.attr('number'),
    system_load_avg05: DS.attr('number'),
    system_load_avg15: DS.attr('number'),
    system_cpu_user: DS.attr('number'),
    system_cpu_system: DS.attr('number'),
    system_cpu_wait: DS.attr('number'),
    system_memory_used: DS.attr('number'),
    system_swap_used: DS.attr('number'),
    collected_at: DS.attr('string'),
    created_at: DS.attr('string'),
    updated_at: DS.attr('string'),
    processes: DS.hasMany('process', {async:true}),

    system_cpu: function() {
        return this.get('system_cpu_user') + this.get('system_cpu_system') + this.get('system_cpu_wait');
    }.property('system_cpu_user', 'system_cpu_system', 'system_cpu_wait'),

    system_memory_used_percent: function() {
        return (this.get('system_memory_used')/this.get('memory_size'))*100;
    }.property('memory_size', 'system_memory_used'),

    system_swap_used_percent: function() {
        return (this.get('system_swap_used')/this.get('swap_size'))*100;
    }.property('swap_size', 'system_swap_used'),

    cpu_user_css_width: function() {
        return 'width:' + this.get('system_cpu_user') + '%';
    }.property('system_cpu_user'),

    cpu_system_css_width: function() {
        return 'width:' + this.get('system_cpu_system') + '%';
    }.property('system_cpu_system'),

    cpu_wait_css_width: function() {
        return 'width:' + this.get('system_cpu_wait') + '%';
    }.property('system_cpu_wait'),

    memory_css_width: function() {
        return 'width:' + this.get('system_memory_used_percent') + '%';
    }.property('system_memory_used_percent'),

    swap_css_width: function() {
        return 'width:' + this.get('system_swap_used_percent') + '%';
    }.property('system_swap_used_percent'),

});
