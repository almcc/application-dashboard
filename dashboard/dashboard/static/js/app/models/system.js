App.System = DS.Model.extend({
    name: DS.attr('string'),
    created_at: DS.attr('string'),
    updated_at: DS.attr('string'),
    processes: DS.hasMany('process'),

    status_ok: function() {
        ok = true;
        var processes = this.get("processes");
        processes.forEach(function(item){
            if(item.get('status_ok') == false){
                ok = false;
            }
        });
        return ok;
    }.property('processes.@each.status_ok'),
});
