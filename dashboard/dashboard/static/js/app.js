Ember.FEATURES["query-params"]=true;

App = Ember.Application.create();

App.ApplicationAdapter = DS.RESTAdapter.extend({
    namespace: 'api/v1',
    coalesceFindRequests: true,
    ajaxError: function(jqXHR) {
        var error = this._super(jqXHR);
        if (jqXHR && jqXHR.status === 400) {
            var response = Ember.$.parseJSON(jqXHR.responseText),
            errors = {},
            keys = Ember.keys(response);
            if (keys.length === 1) {
                var jsonErrors = response[keys[0]];
                Ember.EnumerableUtils.forEach(Ember.keys(jsonErrors), function(key) {
                    errors[key] = jsonErrors[key];
                });
            }
            return new DS.InvalidError(errors);
        } else {
            return error;
        }
    }
});
