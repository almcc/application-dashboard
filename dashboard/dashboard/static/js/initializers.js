var socket = io.connect("/monit");

socket.on('connect', function (msg) {
    console.log('connected to monit socket');  // DEBUG
});

App.initializer({
    name: "load-servers",
    initialize: function(container, application) {
        var store = container.lookup('store:main');
        socket.on('postSaveServer', function (msg) {
            store.push('server', msg);
        });
        socket.on('postDeleteServer', function (msg) {
            var server = store.getById('server', msg.id);
            if (server != null) {
                server.unloadRecord();
            }
        });
        application.deferReadiness();
        store.find('server').then(function(response) {
            Helpers.advanceLoadingBar(25);
            application.advanceReadiness();
        });
    }
});

App.initializer({
    name: "load-processes",
    initialize: function(container, application) {
        var store = container.lookup('store:main');
        socket.on('postSaveProcess', function (msg) {
            store.push('process', msg);
        });
        socket.on('postDeleteProcess', function (msg) {
            var process = store.getById('process', msg.id);
            if (process != null) {
                process.unloadRecord();
            }
        });
        application.deferReadiness();
        store.find('process').then(function(response) {
            Helpers.advanceLoadingBar(25);
            application.advanceReadiness();
        });
    }
});

App.initializer({
    name: "load-systems",
    initialize: function(container, application) {
        var store = container.lookup('store:main');
        socket.on('postSaveSystem', function (msg) {
            store.push('system', msg);
        });
        socket.on('postDeleteSystem', function (msg) {
            var system = store.getById('system', msg.id);
            if (system != null) {
                system.unloadRecord();
            }
        });
        application.deferReadiness();
        store.find('system').then(function(response) {
            Helpers.advanceLoadingBar(25);
            application.advanceReadiness();
        });
    }
});
