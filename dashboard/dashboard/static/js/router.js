App.Router.map(function() {
    this.resource("machines", function() {
        this.resource("servers", function() {
            this.resource("server", {'path': '/:server_id'}, function() {
                this.resource("process", {'path': '/process/:process_id'}, function() {});
                this.route('edit');
            });
            this.route('new');
        });

    });
    this.resource("systems", function() {
        this.resource("system", {'path': '/:system_id'}, function() {
            this.route('edit');
        });
        this.route('new');
    });
});
